libdancer2-plugin-database-perl (2.17-3) unstable; urgency=medium

  * Team upload.
  * stabilize-test-database-connection.patch: new patch. (Closes: #923824)
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.

 -- Étienne Mollier <emollier@debian.org>  Tue, 13 Jun 2023 20:12:49 +0200

libdancer2-plugin-database-perl (2.17-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on
      libdancer-plugin-database-core-perl, libdancer2-perl, libplack-perl.
    + libdancer2-plugin-database-perl: Drop versioned constraint on
      libdancer-plugin-database-core-perl, libdancer2-perl in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 22:45:14 +0100

libdancer2-plugin-database-perl (2.17-1) unstable; urgency=medium

  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Import upstream version 2.17.
  * Bump versioned (build) dependency on
    libdancer-plugin-database-core-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Sep 2016 18:09:09 +0200

libdancer2-plugin-database-perl (2.16-1) unstable; urgency=medium

  * Import upstream version 2.16.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Thu, 28 Apr 2016 17:24:30 +0200

libdancer2-plugin-database-perl (2.15-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Import upstream version 2.15.
  * Make (build) dependency on libdancer2-perl versioned.
  * Declare compliance with Debian Policy 3.9.7.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Feb 2016 02:05:08 +0100

libdancer2-plugin-database-perl (2.14-1) unstable; urgency=medium

  * Import upstream version 2.14.
  * Drop build dependency on libtest-pod-coverage-perl.
    The respective test is marked as a release test now.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Jan 2016 22:05:42 +0100

libdancer2-plugin-database-perl (2.13-1) unstable; urgency=medium

  * Import upstream version 2.13.
  * Update years of packaging copyright.
  * Bump versioned (build) dependency on
    libdancer-plugin-database-core-perl.
  * Drop version from libdancer2-perl (build) dependency.
    Nothing older in the archive.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Oct 2015 15:26:18 +0200

libdancer2-plugin-database-perl (2.12-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 2.12
  * Update years of packaging copyright.
  * Update (build) dependencies.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Oct 2014 15:48:08 +0200

libdancer2-plugin-database-perl (2.10-1) unstable; urgency=low

  * Initial release (closes: #724036).

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Sep 2013 14:33:57 +0200
